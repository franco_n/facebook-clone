import React, { useState } from 'react'
import { Avatar } from '@material-ui/core';
import VideoCamIcon from '@material-ui/icons/Videocam'
import PhotoLibraryIcon from '@material-ui/icons/Photo'
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import './MessageSender.css'
import { useStateValue } from './StateProvider';
import db from './firebase';
import firebase from 'firebase';

function MessageSender() {
    const [{ user }, dispatch] = useStateValue();
    const [input, setInput] = useState('');
    const [imageURL, setImageURL] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        // Some clever DB stuff yeay
        db.collection('posts').add({
            message: input,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            profilePic: user.photoURL,
            username: user.displayName,
            image: imageURL
        });
        setInput('');
        setImageURL('');
    }

    return <div className="messageSender">
            <div className="messageSender__top">
                <Avatar src={user.photoURL} />
                <form>
                    <input className="messageSender__input" placeholder={`What's on your mind ? ${user.displayName} ?`} value={input} onChange={e => setInput(e.target.value)} />
                    <input value={imageURL} onChange={e => setImageURL(e.target.value)} placeholder='Image URL (optional)' />
                    <button onClick={handleSubmit} type='submit'>Handle submit</button>
                </form>
            </div>
            <div className="messageSender__bottom">
                <div className="messageSender__option">
                    <VideoCamIcon style={{color: "red"}} />
                    <h3>Live video</h3>
                </div>
                <div className="messageSender__option">
                    <PhotoLibraryIcon style={{color: "green"}} />
                    <h3>Photo/Videos</h3>
                </div>
                <div className="messageSender__option">
                    <InsertEmoticonIcon style={{ color: "orange"  }} />
                    <h3>Feeling/Activities</h3>
                </div>
            </div>
    </div>
}

export default MessageSender

import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCCUSbXj8_4gfXIq9XKB8SBSEOm70bSGGk",
    authDomain: "clone-app-4d6d2.firebaseapp.com",
    projectId: "clone-app-4d6d2",
    storageBucket: "clone-app-4d6d2.appspot.com",
    messagingSenderId: "484527805447",
    appId: "1:484527805447:web:4092305f155e3fdba086a2",
    measurementId: "G-GB7JY3H7YS"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;

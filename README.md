# Facebook Clone

This project is a simple Facebook-clone written in ReactJS for the front-end part of the application
and the back-end part is a simple Firebase

## Front-end overview

The Front-end of the application is written using ReactJS. This is a library for front-end part of an application and this library itself allows the creator of the application to define the app using a Component-based skeleton of it, for example an application can be divided into several component like:
* Header
* Body
* Footer
And these 3 elements can be component in ReactJS.

## Back-end overview

The back-end of the facebook clone application is created using google [Firebase](https://firebase.google.com/). Firebase is Google’s mobile application development platform that helps you build, improve, and grow your app. Firebase is a toolset to “build, improve, and grow your app”, and the tools it gives you cover a large portion of the services that developers would normally have to build themselves, but don’t really want to build, because they’d rather be focusing on the app experience itself. This includes things like analytics, authentication, databases, configuration, file storage, push messaging, and the list goes on. The services are hosted in the cloud, and scale with little to no effort on the part of the developer.
